# Gitlab CI/CD basic example

<small>Requisito: [Installare Gitlab Runners nel droplet con docker](https://docs.gitlab.com/runner/install/docker.html)</small>

Settare 3 variabili ambientali su **Impostazioni -> CI / CD -> Variables**

```
DOCTL_DROPLET_NAME = nome del droplet su digitalocean
DOCTL_TOKEN = token digitalocean (si crea dal droplet)
SSH_PRIVATE_KEY = chiave ssh dell'utente di digitalocean (~/.ssh/id_rsa)
```

Per effettuale il deploy pushare sul branch `production`
<br/>
Il sito è visibile all'indirizzo [157.230.24.2](http://157.230.24.2/)
