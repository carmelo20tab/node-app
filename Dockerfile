FROM node:8-jessie

WORKDIR /app

COPY ./package-lock.json /app/package-lock.json
COPY ./package.json /app/package.json
COPY ./index.js /app/index.js
COPY ./index.html /app/index.html

RUN npm install

EXPOSE 3000

CMD node index.js
