#!/bin/bash

doctl compute ssh $DOCTL_DROPLET_NAME --ssh-command "docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY"

doctl compute ssh $DOCTL_DROPLET_NAME --ssh-command "if [ "$(docker ps -q -f name=webserver)" ]; then docker stop webserver && docker rm -f webserver; fi;"

doctl compute ssh $DOCTL_DROPLET_NAME --ssh-command "docker run --name webserver -d -p 80:3000 $CI_REGISTRY/$CI_PROJECT_PATH:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
