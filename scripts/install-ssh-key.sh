#!/bin/bash

echo ""
echo "Installing SSH Key"
echo ""

apt-get update && /
apt-get install -y openssh-client

eval $(ssh-agent -s)

echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

mkdir -p ~/.ssh

chmod 700 ~/.ssh

echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa

chmod 400 ~/.ssh/id_rsa

[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

echo ""
echo "DONE"
echo ""
