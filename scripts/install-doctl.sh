#!/bin/bash

DOCTL_VERSION="1.31.2"

echo ""
echo "Installing digitalocean-cli $DOCTL_VERSION"
echo ""

apt-get update && /
apt-get install -y wget && /
apt-get install -y openssh-server

cd /usr/local/src

wget "https://github.com/digitalocean/doctl/releases/download/v$DOCTL_VERSION/doctl-$DOCTL_VERSION-linux-amd64.tar.gz"

tar xvf doctl-$DOCTL_VERSION-linux-amd64.tar.gz

rm -f doctl-$DOCTL_VERSION-linux-amd64.tar.gz

mv doctl /usr/local/bin

echo ""
echo "DONE"
echo ""
